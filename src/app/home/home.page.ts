import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  animations: [
    trigger('vanishState', [
      transition('* => void', [
        animate('500ms linear', style({ opacity: '0' })),
      ]),
    ]),
    trigger('addState', [
      state(
        'idle',
        style({
          opacity: '0.3',
          transform: 'scale(1)',
        })
      ),
      state(
        'adding',
        style({
          opacity: '1',
          transform: 'scale(1.3)',
        })
      ),
      transition('idle <=> adding', animate('300ms linear')),
    ]),
    trigger('itemState', [
      transition('void => *', [
        style({ transform: 'translateX(-100%)' }),
        animate('500ms ease-out'),
      ]),
      transition('* => void', [
        animate('500ms ease-in', style({ transform: 'translateX(100%)' })),
      ]),
    ]),
  ],
})
export class HomePage implements OnInit {
  public showMsg = false;
  public isAdding = 'idle';

  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      this.showMsg = true;
    }, 1000);

    setTimeout(() => {
      this.showMsg = false;
    }, 2000);
  }
}
